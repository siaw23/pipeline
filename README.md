NOTE: There's a `git log` in every folder (frontend, backend) including this particular one.

# Installation

This project has two parts to it. First is the frontend (built with React) and then the backend (built with Rails).

Firstly, clone the project to a convenient location with:

`git clone git@bitbucket.org:siaw23/pipeline.git`

Then with your terminal application `cd` into both "backend" and "frontend" on two seperate tabs.

# Running the Backend

Make sure you have Ruby 2.6.3 installed.

Then in your terminal while `cd`ed into "backend" run `bundle`

You may need to run `rails db:create` in case you get an error along the liness of "database "backend_development" does not exist"

Before moving on, create a file ".env" in the "backend" root folder. The content of this file should be `PIPELINE_KEY=XXX` where "XXX" is your API key.

Then run `rails s` to start the server. At this point you can visity `http://localhost:4000/pipelinedeals` in your browser to confirm this endpoint displays some JSON data of deals.

# Running the Frontend

`cd` now into the "frontend" folder in the terminal (in the another terminal tab).
Run `yarn install && yarn start`. If you don't have Yarn please [seek help](https://yarnpkg.com/lang/en/docs/install/#mac-stable). This should open your default browser with a column chart representing the JSON data you saw earlier at `http://localhost:4000/pipelinedeals`.