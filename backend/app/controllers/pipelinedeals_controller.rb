class PipelinedealsController < ApplicationController
  def index
    base_uri = "https://api.pipelinedeals.com/"

    response = HTTParty.get(base_uri + "api/v3/deals.json?api_key=#{ENV['PIPELINE_KEY']}")

    render json: response.body
  end
end
