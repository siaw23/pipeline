require 'rails_helper'

RSpec.describe PipelinedealsController, type: :controller do
  describe 'GET index' do
    before(:each) do
      get :index
      @response = JSON.parse(@response.body)
    end

    it 'fetches all PipelineDeals' do
      expect(@response['entries'].count).to eq(100)
    end

    it 'fetches first iteam' do
      expect(response['entries'][0]['name']).to eq('King-Cremin')
    end

    it 'correctly fetches entry value' do
      expect(response['entries'][0]['value']).to eq('44049.04')
    end

    it 'correctly fethes deal stage percentage' do
      expect(response['entries'][0]['deal_stage']['percent']).to eq(50)
    end
  end
end
