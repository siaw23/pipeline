import React, {useState, useEffect} from 'react';
import Chartkick, { ColumnChart } from 'react-chartkick'
import Highcharts from 'highcharts'
import 'chart.js'
import axios from 'axios';
import './App.css';

Chartkick.use(Highcharts)

function groupDealsByName(deals) {
  return deals.reduce((acc, deal) => {
    const dealStageName = deal.deal_stage.name;

    if (acc[dealStageName]) {
     acc[dealStageName].push({
      value: deal.value,
      percentage: deal.deal_stage.percent
    });
     } else {
      acc[dealStageName] = [{
        value: deal.value,
        percentage: deal.deal_stage.percent
      }];
    }
  return acc;
  }, {});
}

function summationOfEveryDealStages(dealStages) {
  return Object.keys(dealStages).map((stageName) => {
    return {
      name: stageName,
      value: Math.round(
        dealStages[stageName].reduce((acc, cur) => acc + parseFloat(cur.value), 0.0)
        ),
      percentage: dealStages[stageName][0].percentage
    }
  })
}

function BarChartComponent({deals}) {

  const data = summationOfEveryDealStages(groupDealsByName(deals))
    .map(aryItem => {
    return [aryItem.percentage, aryItem.value]
  });

  return <ColumnChart data={data} />
}

function App() {
  const [deals, setDeals] = useState(null);

  useEffect(() => {
    axios
      .get('/pipelinedeals')
      .then(resp => setDeals(resp.data.entries))
      .catch(err => console.log(err))
  }, []);

  if (deals === null) {
    return <p>Fetching deals...</p>;
  }


  return (
    <div className="App">
      <div id="barChart">
        <BarChartComponent deals={deals}/>
      </div>
    </div>
    );
}

export default App;
